import { AlumnoComponent } from './alumno/alumno.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AlumnoInputComponent } from './alumno/alumno-input.component';
import { AppRoutingModule } from './alumno/app.routing.module';

@NgModule({
  declarations: [
    AppComponent,
    AlumnoComponent,
    AlumnoInputComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
