import { Component, Input}  from "@angular/core";
import { Subscription } from 'rxjs';
import { AlumnoService } from './alumno.service';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html',
  styleUrls: ['./alumno.component.css']
})
export class AlumnoComponent{
  private alumnosSubs: Subscription;
  activo: boolean=true;
  //alumnos:string[]=["Miguel", "Adrian", "Ana" ];
  //@Input() alumnos:string[]=["Miguel", "Adrian", "Ana" ];
  alumnos:string[];

  constructor(private service: AlumnoService){
    //this.alumnos = service.alumnosDummy;
  }

  ngOnInit(){
    //this.alumnos = this.service.alumnos;
    this.service.fetchNombres();
    this.alumnosSubs = this.service.alumnosChanged.subscribe(alumnos => {
      this.alumnos = alumnos;
    })
  }
  ngOnDestroy(){
    this.alumnosSubs.unsubscribe();
  }

  onRemoveAlumno(name:string){
      this.service.removeAlumno(name);
  }

  onClickActivar(){
    this.activo=!this.activo;
  }
}
