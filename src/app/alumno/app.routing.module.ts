import { AlumnoComponent } from './alumno.component';
import { NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router'
import { AlumnoInputComponent } from './alumno-input.component';

const routes: Routes = [
  {path: '', component: AlumnoComponent},
  {path: 'input', component:AlumnoInputComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
