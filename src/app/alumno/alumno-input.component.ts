import {Component, EventEmitter, Output} from "@angular/core";
import { AlumnoService } from './alumno.service';

@Component({
  selector: 'app-alumno-input',
  templateUrl: 'alumno-input.component.html',
  styleUrls: ['alumno-input.component.css']
})

export class AlumnoInputComponent{
  @Output() addAlumno = new EventEmitter<string>();
  nombre: string='';

  constructor(private service: AlumnoService){}


  alta(){
    //this.addAlumno.emit(this.nombre);
    this.service.addAlumno(this.nombre);
    this.nombre;
  }
}
